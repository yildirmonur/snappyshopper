//
//  SnappyShopperTestCaseTests.swift
//  SnappyShopperTestCaseTests
//
//  Created by Onur Yıldırım on 27.09.2021.
//

import XCTest
@testable import SnappyShopperTestCase

class SnappyShopperTestCaseTests: XCTestCase {

    var searchViewModel: TeamPlayerSearchViewModel!
    
    override func setUp() {
        super.setUp()
        
        
    }

    override func tearDown() {
        searchViewModel = .none
    }
    
    func testShowMorePlayers() {
        let searchCoordinator = TeamPlayerSearchCoordinator(service: Mock10PlayersResultService(), parent: .none)
        searchViewModel = searchCoordinator.viewModel
        searchViewModel.getSearchResults(with: "mock")
        XCTAssertEqual(searchViewModel.showMorePlayers, .loadMore)
    }
    
    func testHideMorePlayersForNonFullResult() {
        let searchCoordinator = TeamPlayerSearchCoordinator(service: Mock9PlayersResultService(), parent: .none)
        searchViewModel = searchCoordinator.viewModel
        searchViewModel.getSearchResults(with: "mock")
        XCTAssertEqual(searchViewModel.showMorePlayers, .allLoaded)
    }
    
    func testHideMorePlayersForNoResult() {
        let searchCoordinator = TeamPlayerSearchCoordinator(service: MockNoPlayerResultService(), parent: .none)
        searchViewModel = searchCoordinator.viewModel
        searchViewModel.getSearchResults(with: "mock")
        XCTAssertEqual(searchViewModel.showMorePlayers, .allLoaded)
    }
}
