//
//  TeamPlayerSearchApi.swift
//  SnappyShopperTestCase
//
//  Created by Onur Yıldırım on 23.09.2021.
//

import Foundation
import Combine

protocol TeamPlayerSearchServiceProtocol: AnyObject {
    
    // async await style
    
    @available(iOS 15.0, *)
    func search(with request: TeamPlayerSearchRequest) async throws -> TeamPlayer
    
    // Combine style
    
    func search(with request: TeamPlayerSearchRequest) -> AnyPublisher<TeamPlayer, APError>
    
    @available(*, deprecated, message: "Use combine style instead")
    func search(with requestModel: TeamPlayerSearchRequest,
                completed: @escaping (Swift.Result<TeamPlayer, APError>) -> Void)
}

final class TeamPlayerSearchService: TeamPlayerSearchServiceProtocol {
    
    @available(iOS 15.0, *)
    func search(with request: TeamPlayerSearchRequest) async throws -> TeamPlayer {
        let urlComponent = URLService.getBaseUrlComponents(path: "/api/football/1.0/search")
        guard let url = urlComponent.url else {
            throw APError.invalidURL
        }
        
        let baseService = BaseWebService()
        return try await baseService.request(url: url, type: .post, body: request)
    }
    
    func search(with request: TeamPlayerSearchRequest) -> AnyPublisher<TeamPlayer, APError> {
        let urlComponent = URLService.getBaseUrlComponents(path: "/api/football/1.0/search")
        guard let url = urlComponent.url else { return Fail(error: APError.invalidURL).eraseToAnyPublisher() }
        
        let baseService = BaseWebService()
        return baseService.request(url: url, type: .post, body: request)
    }
    
    
    func search(
        with requestModel: TeamPlayerSearchRequest, completed: @escaping (Swift.Result<TeamPlayer, APError>) -> Void
    ) {
        let urlComponent = URLService.getBaseUrlComponents(path: "/api/football/1.0/search")
        guard let url = urlComponent.url else { return }
        let baseService = BaseWebService()
        baseService.request(url: url, type: .post, body: requestModel, completed: completed)
    }
}

final class Mock10PlayersResultService: TeamPlayerSearchServiceProtocol {
    
    func search(with request: TeamPlayerSearchRequest) async throws -> TeamPlayer {
        if let searchResultFilePath = Bundle.main.path(forResource: "10PlayersResult", ofType: "json"),
           let searchResultData = FileManager.default.contents(atPath: searchResultFilePath) {
            do {
                let decoder = JSONDecoder()
                let decodedResponse = try decoder.decode(TeamPlayer.self, from: searchResultData)
                
                return decodedResponse
            } catch {
                print(error)
                print(String(decoding: searchResultData, as: UTF8.self))
            }
        }
        throw APError.unableToComplete
    }
    
    func search(with request: TeamPlayerSearchRequest) -> AnyPublisher<TeamPlayer, APError> {
        if let searchResultFilePath = Bundle.main.path(forResource: "10PlayersResult", ofType: "json"),
           let searchResultData = FileManager.default.contents(atPath: searchResultFilePath) {
            do {
                let decoder = JSONDecoder()
                let decodedResponse = try decoder.decode(TeamPlayer.self, from: searchResultData)
                
                return Swift.Result.success(decodedResponse).publisher.eraseToAnyPublisher()
            } catch {
                print(error)
                print(String(decoding: searchResultData, as: UTF8.self))
            }
        }
        return Fail(error: APError.unableToComplete).eraseToAnyPublisher()
    }
    
    func search(with requestModel: TeamPlayerSearchRequest,
                completed: @escaping (Swift.Result<TeamPlayer, APError>) -> Void) {
        if let searchResultFilePath = Bundle.main.path(forResource: "10PlayersResult", ofType: "json"),
           let searchResultData = FileManager.default.contents(atPath: searchResultFilePath) {
            do {
                let decoder = JSONDecoder()
                let decodedResponse = try decoder.decode(TeamPlayer.self, from: searchResultData)
                completed(.success(decodedResponse))
            } catch {
                print(error)
                print(String(decoding: searchResultData, as: UTF8.self))
                completed(.failure(.invalidData))
            }
        }
    }
}

final class Mock9PlayersResultService: TeamPlayerSearchServiceProtocol {
    
    func search(with request: TeamPlayerSearchRequest) async throws -> TeamPlayer {
        if let searchResultFilePath = Bundle.main.path(forResource: "9PlayersResult", ofType: "json"),
           let searchResultData = FileManager.default.contents(atPath: searchResultFilePath) {
            do {
                let decoder = JSONDecoder()
                let decodedResponse = try decoder.decode(TeamPlayer.self, from: searchResultData)
                
                return decodedResponse
            } catch {
                print(error)
                print(String(decoding: searchResultData, as: UTF8.self))
            }
        }
        throw APError.unableToComplete
    }
    
    func search(with request: TeamPlayerSearchRequest) -> AnyPublisher<TeamPlayer, APError> {
        if let searchResultFilePath = Bundle.main.path(forResource: "9PlayersResult", ofType: "json"),
           let searchResultData = FileManager.default.contents(atPath: searchResultFilePath) {
            do {
                let decoder = JSONDecoder()
                let decodedResponse = try decoder.decode(TeamPlayer.self, from: searchResultData)
                
                return Swift.Result.success(decodedResponse).publisher.eraseToAnyPublisher()
            } catch {
                print(error)
                print(String(decoding: searchResultData, as: UTF8.self))
            }
        }
        return Fail(error: APError.unableToComplete).eraseToAnyPublisher()
    }
    
    func search(with requestModel: TeamPlayerSearchRequest,
                completed: @escaping (Swift.Result<TeamPlayer, APError>) -> Void) {
        if let searchResultFilePath = Bundle.main.path(forResource: "9PlayersResult", ofType: "json"),
           let searchResultData = FileManager.default.contents(atPath: searchResultFilePath) {
            do {
                let decoder = JSONDecoder()
                let decodedResponse = try decoder.decode(TeamPlayer.self, from: searchResultData)
                completed(.success(decodedResponse))
            } catch {
                print(error)
                print(String(decoding: searchResultData, as: UTF8.self))
                completed(.failure(.invalidData))
            }
        }
    }
}

final class MockNoPlayerResultService: TeamPlayerSearchServiceProtocol {
    
    func search(with request: TeamPlayerSearchRequest) async throws -> TeamPlayer {
        if let searchResultFilePath = Bundle.main.path(forResource: "NoResult", ofType: "json"),
           let searchResultData = FileManager.default.contents(atPath: searchResultFilePath) {
            do {
                let decoder = JSONDecoder()
                let decodedResponse = try decoder.decode(TeamPlayer.self, from: searchResultData)
                
                return decodedResponse
            } catch {
                print(error)
                print(String(decoding: searchResultData, as: UTF8.self))
            }
        }
        throw APError.unableToComplete
    }
    
    func search(with request: TeamPlayerSearchRequest) -> AnyPublisher<TeamPlayer, APError> {
        if let searchResultFilePath = Bundle.main.path(forResource: "NoResult", ofType: "json"),
           let searchResultData = FileManager.default.contents(atPath: searchResultFilePath) {
            do {
                let decoder = JSONDecoder()
                let decodedResponse = try decoder.decode(TeamPlayer.self, from: searchResultData)
                
                return Swift.Result.success(decodedResponse).publisher.eraseToAnyPublisher()
            } catch {
                print(error)
                print(String(decoding: searchResultData, as: UTF8.self))
            }
        }
        return Fail(error: APError.unableToComplete).eraseToAnyPublisher()
    }
    
    func search(with requestModel: TeamPlayerSearchRequest,
                completed: @escaping (Swift.Result<TeamPlayer, APError>) -> Void) {
        if let searchResultFilePath = Bundle.main.path(forResource: "NoResult", ofType: "json"),
           let searchResultData = FileManager.default.contents(atPath: searchResultFilePath) {
            do {
                let decoder = JSONDecoder()
                let decodedResponse = try decoder.decode(TeamPlayer.self, from: searchResultData)
                completed(.success(decodedResponse))
            } catch {
                print(error)
                print(String(decoding: searchResultData, as: UTF8.self))
                completed(.failure(.invalidData))
            }
        }
    }
}
