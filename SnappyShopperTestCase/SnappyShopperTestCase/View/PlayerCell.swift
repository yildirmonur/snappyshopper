//
//  SectionView.swift
//  SnappyShopperTestCase
//
//  Created by Onur Yıldırım on 24.09.2021.
//

import SwiftUI

struct PlayerCell: View {
    
    var isFavorite: Bool
    var title: String
    var subtitle: String
    var secondSubtitle: String
    var nationalFlag: String?
    
    var body: some View {
        HStack {
            Image(systemName: isFavorite ? "star.fill" : "star")
                .resizable()
                .scaledToFit()
                .frame(height: 30)
                .cornerRadius(4)
                .padding(.vertical, 4)
            
            VStack(spacing: 5) {
                HStack {
                    Text(title)
                    Spacer()
                }
                .padding(.leading, 10)
                .padding(.trailing, 10)
                .padding(.top, 10)
                
                HStack() {
                    Text(subtitle)
                        .padding(.trailing, 20)
                    Text(secondSubtitle)
                    Spacer()
                }
                .padding(.leading, 10)
                .padding(.trailing, 10)
                .padding(.bottom, 10)
            }
            
            Spacer()
            
            if let nationalFlag = nationalFlag {
                Image(nationalFlag)
                    .resizable()
                    .scaledToFit()
                    .frame(height: 30)
                    .padding(.vertical, 4)
            }
        }
        
    }
}
