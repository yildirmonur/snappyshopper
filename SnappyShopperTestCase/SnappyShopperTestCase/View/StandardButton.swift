//
//  StandardButton.swift
//  StandardButton
//
//  Created by Lojika IOS on 27.08.2021.
//

import SwiftUI

struct StandardButton: View {
    
    var title: String
    
    var body: some View {
        Text(title)
            .bold()
            .font(.title)
            .frame(maxWidth: .infinity)
            .frame(height: 50)
            .background(Color(.systemRed))
            .foregroundColor(.white)
            .cornerRadius(10)
            .padding(.horizontal)
    }
}
