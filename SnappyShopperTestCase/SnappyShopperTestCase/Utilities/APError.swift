//
//  APError.swift
//  SnappyShopperTestCase
//
//  Created by Onur Yıldırım on 23.09.2021.
//


import Foundation

enum APError: Error {
    case invalidURL
    case unableToComplete
    case invalidResponse
    case invalidData
}
