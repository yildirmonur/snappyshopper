//
//  BaseWebService.swift
//  SnappyShopperTestCase
//
//  Created by Onur Yıldırım on 24.09.2021.
//

import Foundation
import Combine

class BaseWebService {
    
    // async await style
    
    @available(iOS 15.0, *)
    func request<T: Decodable, M: Encodable> (url: URL, type: URLService.RequestType, body: M) async throws -> T {
        var request = URLRequest(url: url)
        request.httpMethod = type.rawValue
        
        do {
            let jsonBody = try JSONEncoder().encode(body)
            request.httpBody = jsonBody
            
            let result: (data: Data, response: URLResponse) = try await URLSession.shared.data(for: request)
            
            let decoder = JSONDecoder()
            return try decoder.decode(T.self, from: result.data)
        }
        catch {
            throw APError.unableToComplete
        }
    }
    
    // Combine style (dataTaskPublisher)
    
    func request<T: Decodable, M: Encodable> (url: URL, type: URLService.RequestType, body: M) -> AnyPublisher<T, APError> {
        var request = URLRequest(url: url)
        request.httpMethod = type.rawValue
        
        do {
            let jsonBody = try JSONEncoder().encode(body)
            request.httpBody = jsonBody
        }
        catch {
            return Fail(error: APError.unableToComplete).eraseToAnyPublisher()
        }
        
        return URLSession.shared.dataTaskPublisher(for: request).map { $0.data }
            .decode(type: T.self, decoder: JSONDecoder())
            .receive(on: RunLoop.main)
            .mapError { _ in .unableToComplete }
            .eraseToAnyPublisher()
    }
    
    // Old style dataTask
    
    func request<T: Decodable, M: Encodable> (
        url: URL, type: URLService.RequestType, body: M, completed: @escaping (Swift.Result<T, APError>) -> Void
    ) {
        var request = URLRequest(url: url)
        request.httpMethod = type.rawValue
        
        do {
            let jsonBody = try JSONEncoder().encode(body)
            request.httpBody = jsonBody
        } catch { print(error) }
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            DispatchQueue.main.async {
                if let _ =  error {
                    completed(.failure(.unableToComplete))
                    return
                }
                
                guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                    completed(.failure(.invalidResponse))
                    return
                }
                
                guard let data = data else {
                    completed(.failure(.invalidData))
                    return
                }
                
                do {
                    let decoder = JSONDecoder()
                    let decodedResponse = try decoder.decode(T.self, from: data)
                    completed(.success(decodedResponse))
                } catch {
                    print(error)
                    print(String(decoding: data, as: UTF8.self))
                    completed(.failure(.invalidData))
                }
            }
        }
        
        task.resume()
    }
}
