//
//  Persistence.swift
//  SnappyShopperTestCase
//
//  Created by Onur Yıldırım on 23.09.2021.
//

import CoreData

struct PersistenceController {
    static let shared = PersistenceController()
    
    private let playerModelEntity = "PlayerModel"
    
    func fetchPlayers(completion: @escaping ([Player]) -> Void) {
        fetchPlayerModels { playerModels in
            var players: [Player] = []
            for playerModel in playerModels {
                players.append(
                    Player(playerID: playerModel.playerID,
                           playerFirstName: playerModel.playerFirstName,
                           playerSecondName: playerModel.playerSecondName,
                           playerNationality: playerModel.playerNationality,
                           playerAge: playerModel.playerAge,
                           playerClub: playerModel.playerClub)
                )
            }
            completion(players)
        }
    }
    
    func save(with player: Player, completion: @escaping (Bool) -> Void) {
        fetchPlayerModels { players in
            // Update by removing old value if player already saved before
            for playerModel in players {
                if playerModel.playerID == player.playerID {
                    delete(with: playerModel, completion: { _ in })
                }
            }
            
            container.viewContext.automaticallyMergesChangesFromParent = true
            container.performBackgroundTask { context in
                let playerModel = PlayerModel(context: context)
                playerModel.playerID = player.playerID
                playerModel.playerAge = player.playerAge
                playerModel.playerClub = player.playerClub
                playerModel.playerFirstName = player.playerFirstName
                playerModel.playerSecondName = player.playerSecondName
                playerModel.playerNationality = player.playerNationality
                
                do {
                    try context.save()
                    completion(true)
                } catch {
                    print("Failure to save context: \(error)")
                    completion(false)
                }
            }
        }
    }
    
    func delete(with player: Player, completion: @escaping (Bool) -> Void) {
        fetchPlayerModels { playerModels in
            for playerModel in playerModels {
                if player.playerID == playerModel.playerID {
                    delete(with: playerModel, completion: completion)
                }
            }
        }
    }
    
    static var preview: PersistenceController = {
        let result = PersistenceController()
        return result
    }()
    
    let container: NSPersistentContainer
    
    init() {
        
        container = NSPersistentContainer(name: "PlayerDataModel")
        
        container.loadPersistentStores { description, error in
            if let error = error {
                fatalError("Error: \(error.localizedDescription)")
            }
        }
    }
}

private extension PersistenceController {
    
    private func fetchPlayerModels(completion: @escaping ([PlayerModel]) -> Void) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: playerModelEntity)
        
        container.viewContext.automaticallyMergesChangesFromParent = true
        container.performBackgroundTask { context in
            do {
                if let fetchResults = try context.fetch(fetchRequest) as? [NSManagedObject],
                   let playerModels = fetchResults as? [PlayerModel] {
                    completion(playerModels)
                }
            } catch {
                print("Fetch failed: Error \(error.localizedDescription)")
                completion([])
            }
        }
    }
    
    func delete(with playerModel: PlayerModel, completion: @escaping (Bool) -> Void) {
        container.viewContext.automaticallyMergesChangesFromParent = true
        container.performBackgroundTask { context in
            context.delete(playerModel)
            if let _ = try? context.save() {
                completion(true)
            }
            else {
                completion(false)
            }
        }
    }
}
