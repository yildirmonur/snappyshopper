//
//  SnappyShopperTestCaseApp.swift
//  SnappyShopperTestCase
//
//  Created by Onur Yıldırım on 23.09.2021.
//

import SwiftUI

@main
struct SnappyShopperTestCaseApp: App {
    
    @StateObject var coordinator = MainCoordinator(searchService: TeamPlayerSearchService())
    
    var body: some Scene {
        WindowGroup {
            MainCoordinatorView(coordinator: coordinator)
        }
    }
}
