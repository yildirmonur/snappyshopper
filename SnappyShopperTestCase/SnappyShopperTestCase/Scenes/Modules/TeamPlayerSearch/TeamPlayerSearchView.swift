//
//  TeamPlayerSearchView.swift
//  SnappyShopperTestCase
//
//  Created by Onur Yıldırım on 23.09.2021.
//

import SwiftUI

struct TeamPlayerSearchView: View {
    
    // MARK: Stored Properties
    
    @ObservedObject var viewModel: TeamPlayerSearchViewModel
    
    // MARK: Views
    
    var body: some View {
        VStack {
            SearchBar(text: $viewModel.searchText)
            if viewModel.isLoading {
                ProgressView()
                Spacer()
            }
            else {
                results
            }
        }
        .onAppear { viewModel.checkLoadedPlayersFavoriteState() }
    }
    
    @ViewBuilder
    private var results: some View {
        List {
            if viewModel.players.count > 0 {
                Section(
                    header: Text("Players")
                        .padding()
                ) {
                    ForEach(viewModel.players, id: \.playerID) { player in
                        PlayerCell(
                            isFavorite: player.isFavorite,
                            title: "\(player.playerFirstName ?? "") \(player.playerSecondName ?? "")",
                            subtitle: "Age: \(player.playerAge ?? "?")",
                            secondSubtitle: "Club: \(player.playerClub ?? "?")",
                            nationalFlag: player.playerNationality
                        )
                        .onTapGesture {
                            viewModel.onChangeFavorite(with: player)
                        }
                    }
                }
                
                if viewModel.showMorePlayers == .loadMore {
                    Button {
                        viewModel.onShowMorePlayers()
                    } label: {
                        StandardButton(title: "More Players")
                    }
                    .buttonStyle(BorderlessButtonStyle())
                }
                else if viewModel.showMorePlayers == .loading {
                    HStack {
                        Spacer()
                        ProgressView()
                        Spacer()
                    }
                }
            }
            if viewModel.teams.count > 0 {
                Section(
                    header: Text("Teams")
                        .padding()
                ) {
                    ForEach(viewModel.teams, id: \.teamID) { team in
                        TeamCell(
                            title: "\(team.teamName ?? "")",
                            subtitle: "City: \(team.teamCity ?? "?")",
                            secondSubtitle: "Stadium: \(team.teamStadium ?? "?")"
                        )
                    }
                }
                if viewModel.showMoreTeams == .loadMore {
                    Button {
                        viewModel.onShowMoreTeams()
                    } label: {
                        StandardButton(title: "More Teams")
                    }
                    .buttonStyle(BorderlessButtonStyle())
                }
                else if viewModel.showMorePlayers == .loading {
                    HStack {
                        Spacer()
                        ProgressView()
                        Spacer()
                    }
                }
            }
        }
    }
}

