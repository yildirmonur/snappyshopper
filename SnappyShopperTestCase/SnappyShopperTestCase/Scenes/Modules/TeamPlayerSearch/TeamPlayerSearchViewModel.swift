//
//  TeamPlayerSearchViewModel.swift
//  SnappyShopperTestCase
//
//  Created by Onur Yıldırım on 23.09.2021.
//

import Foundation
import Combine

final class TeamPlayerSearchViewModel: ObservableObject {
    
    private var cancellable = Set<AnyCancellable>()
    
    @Published var searchText = "" {
        didSet {
            offsets = (0, 0)
            
            if searchText.isEmpty {
                teamsPlayers = .none
                isLoading = false
            }
            else {
                isLoading = true
            }
        }
    }
    @Published var debouncedText = "" {
        didSet {
            if !debouncedText.isEmpty {
                getSearchResults(with: debouncedText)
            }
        }
    }
    private var subscriptions = Set<AnyCancellable>()
    
    private var teamsPlayers: Result? {
        didSet {
            if let teamsPlayers = teamsPlayers {
                teams = teamsPlayers.teams ?? []
                checkFavoritePlayers(players: teamsPlayers.players ?? []) { [weak self] updatedPlayers in
                    self?.players = updatedPlayers
                }
            }
        }
    }
    
    @Published var teams: [Team] = [] {
        didSet {
            if teams.count % 10 != 0 || teams.count == 0 {
                showMoreTeams = .allLoaded
            }
            else {
                showMoreTeams = .loadMore
            }
        }
    }
    @Published var players: [Player] = [] {
        didSet {
            if players.count % 10 != 0 || players.count == 0 {
                showMorePlayers = .allLoaded
            }
            else {
                showMorePlayers = .loadMore
            }
        }
    }
    @Published var isLoading = false
    @Published var showMorePlayers = LoadType.loadMore
    @Published var showMoreTeams = LoadType.loadMore
    private var offsets = (0, 0)
    private let searchService: TeamPlayerSearchServiceProtocol
    private unowned let coordinator: TeamPlayerSearchCoordinator
    
    init(coordinator: TeamPlayerSearchCoordinator,
         searchService: TeamPlayerSearchServiceProtocol) {
        self.coordinator = coordinator
        self.searchService = searchService
        
        $searchText
            .debounce(for: .seconds(0.4), scheduler: DispatchQueue.main)
            .sink(receiveValue: { t in
                self.debouncedText = t
            } )
            .store(in: &subscriptions)
    }
    
    func checkLoadedPlayersFavoriteState() {
        checkFavoritePlayers(players: players) { [weak self] updatedPlayers in
            self?.players = updatedPlayers
        }
    }
    
    func onShowMorePlayers() {
        showMorePlayers = .loading
        offsets.0 += 10
        getSearchResults(with: debouncedText, searchType: .players(offsets.0))
    }
    
    func onShowMoreTeams() {
        showMoreTeams = .loading
        offsets.1 += 10
        getSearchResults(with: debouncedText, searchType: .teams(offsets.1))
    }
    
    func onChangeFavorite(with player: Player) {
        guard let index = players.firstIndex(where: { $0.playerID == player.playerID }) else { return }
        
        if player.isFavorite {
            PersistenceController.shared.delete(with: player) { [weak self] success in
                DispatchQueue.main.async {
                    if success { self?.players[index].isFavorite = false }
                }
            }
        }
        else {
            PersistenceController.shared.save(with: player) { [weak self] success in
                DispatchQueue.main.async {
                    if success { self?.players[index].isFavorite = true }
                }
            }
        }
    }
    
    func getSearchResults(with text: String, searchType: TeamPlayerSearchRequest.SearchType = .all) {
        let request = TeamPlayerSearchRequest(searchString: debouncedText, searchType: searchType)
        
        searchService.search(with: request).sink { [unowned self] completion in
            isLoading = false
            
            switch completion {
            case .failure(let error):
                coordinator.showUrlAlert(error: error)
            case .finished:
                break
            }
        } receiveValue: { [unowned self] result in
            
            switch searchType {
            case .all:
                teamsPlayers = result.result
                
            case .players(_):
                guard let result = result.result, let players = result.players else { return }
                checkFavoritePlayers(players: players) { [weak self] updatedPlayers in
                    self?.players += updatedPlayers
                }
                
            case .teams(_):
                guard let result = result.result, let teams = result.teams else { return }
                self.teams += teams
            }
        }
        .store(in: &cancellable)
    }
}

private extension TeamPlayerSearchViewModel {
    
    func checkFavoritePlayers(players: [Player], completion: @escaping ([Player]) -> Void) {
        var tempPlayers = players
        PersistenceController.shared.fetchPlayers { favoritePlayers in
            DispatchQueue.main.async {
                for (index, player) in tempPlayers.enumerated() {
                    tempPlayers[index].isFavorite = favoritePlayers.first(where: { $0.playerID == player.playerID }) != nil
                }
                
                completion(tempPlayers)
            }
        }
    }
}

extension TeamPlayerSearchViewModel {
    
    enum LoadType {
        case loadMore
        case loading
        case allLoaded
    }
}
