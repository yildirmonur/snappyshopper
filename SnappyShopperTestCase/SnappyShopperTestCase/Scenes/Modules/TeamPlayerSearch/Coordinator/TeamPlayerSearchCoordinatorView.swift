//
//  TeamPlayerSearchCoordinatorView.swift
//  SnappyShopperTestCase
//
//  Created by Onur Yıldırım on 23.09.2021.
//

import SwiftUI

struct TeamPlayerSearchCoordinatorView: View {
    
    // MARK: Stored Properties
    
    @ObservedObject var coordinator: TeamPlayerSearchCoordinator
    
    // MARK: Views
    
    var body: some View {
        TeamPlayerSearchView(viewModel: coordinator.viewModel)
    }
}
