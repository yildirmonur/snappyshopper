//
//  TeamPlayerSearchCoordinator.swift
//  SnappyShopperTestCase
//
//  Created by Onur Yıldırım on 23.09.2021.
//

import SwiftUI

final class TeamPlayerSearchCoordinator: ObservableObject, Identifiable {
    
    @Published var viewModel: TeamPlayerSearchViewModel!
    @Published var favoritePlayersViewModel: FavoritePlayersViewModel?
    
    private unowned let parent: MainCoordinator?
    
    init(service: TeamPlayerSearchServiceProtocol, parent: MainCoordinator?) {
        self.parent = parent
        self.viewModel = TeamPlayerSearchViewModel(coordinator: self,
                                                   searchService: service)
    }
    
    func showUrlAlert(error: APError) {
        parent?.showUrlAlert(error: error)
    }
}
