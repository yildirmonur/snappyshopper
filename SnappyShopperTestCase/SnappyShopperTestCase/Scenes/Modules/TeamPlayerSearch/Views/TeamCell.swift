//
//  TeamCell.swift
//  SnappyShopperTestCase
//
//  Created by Onur Yıldırım on 24.09.2021.
//

import SwiftUI

struct TeamCell: View {

    var title: String
    var subtitle: String
    var secondSubtitle: String

    var body: some View {
        VStack(alignment: .leading, spacing: 5) {
            Text(title)
            Text(subtitle)
            Text(secondSubtitle)
        }
        .padding()
    }
}

