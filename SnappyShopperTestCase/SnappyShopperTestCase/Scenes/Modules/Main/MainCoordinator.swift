//
//  MainCoordinator.swift
//  SnappyShopperTestCase
//
//  Created by Onur Yıldırım on 23.09.2021.
//

import Foundation
import SwiftUI

enum MainTab {
    case search
    case favorite
}

class MainCoordinator: ObservableObject {

    // MARK: Stored Properties
    
    @Published var alertItem: AlertItem?
    @Published var tab = MainTab.search
    @Published var searchCoordinator: TeamPlayerSearchCoordinator!
    @Published var favoriteCoordinator: FavoritePlayersCoordinator
    
    private let searchService: TeamPlayerSearchServiceProtocol
    
    init(searchService: TeamPlayerSearchServiceProtocol) {
        self.searchService = searchService
        self.favoriteCoordinator = .init(title: "Favorite Players")
        
        self.searchCoordinator = .init(service: searchService, parent: self)
    }
    
    func showUrlAlert(error: APError) {
        switch error {
        case .invalidData:
            alertItem = AlertContext.invalidData
            
        case .invalidURL:
            alertItem = AlertContext.invalidURL
            
        case .invalidResponse:
            alertItem = AlertContext.invalidResponse
            
        case .unableToComplete:
            alertItem = AlertContext.unableToComplete
        }
    }
}


