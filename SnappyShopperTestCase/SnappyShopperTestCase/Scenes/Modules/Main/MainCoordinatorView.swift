//
//  MainCoordinatorView.swift
//  SnappyShopperTestCase
//
//  Created by Onur Yıldırım on 23.09.2021.
//

import SwiftUI

struct MainCoordinatorView: View {
    
    // MARK: Stored Properties
    
    @ObservedObject var coordinator: MainCoordinator
    
    // MARK: Views
    
    var body: some View {
        TabView(selection: $coordinator.tab) {
            TeamPlayerSearchCoordinatorView(
                coordinator: coordinator.searchCoordinator
            )
            .tabItem { Label("Search", systemImage: "doc.text.magnifyingglass") }
            .tag(MainTab.search)
            
            FavoritePlayersCoordinatorView(
                coordinator: coordinator.favoriteCoordinator
            )
            .tabItem { Label("Favorite", systemImage: "star.fill") }
            .tag(MainTab.favorite)
        }
        .alert(item: $coordinator.alertItem) {
            Alert(title: $0.title, message: $0.message, dismissButton: $0.dismissButton)
        }
    }
    
}

