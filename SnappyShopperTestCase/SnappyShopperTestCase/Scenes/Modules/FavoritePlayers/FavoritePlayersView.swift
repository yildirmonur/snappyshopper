//
//  FavoritePlayersView.swift
//  SnappyShopperTestCase
//
//  Created by Onur Yıldırım on 23.09.2021.
//

import SwiftUI

struct FavoritePlayersView: View {
    
    // MARK: Stored Properties
    
    @ObservedObject var viewModel: FavoritePlayersViewModel
    
    // MARK: Views
    
    var body: some View {
        List {
            ForEach(viewModel.players, id: \.playerID) { player in
                PlayerCell(
                    isFavorite: true,
                    title: "\(player.playerFirstName ?? "") \(player.playerSecondName ?? "")",
                    subtitle: "Age: \(player.playerAge ?? "?")",
                    secondSubtitle: "Club: \(player.playerClub ?? "?")",
                    nationalFlag: player.playerNationality
                )
            }
            .onDelete(perform: viewModel.delete)
        }
        .toolbar {
            EditButton()
        }
        .onAppear { viewModel.fetchFavoritePlayers() }
        .navigationBarTitle(viewModel.title, displayMode: .inline)
    }
}
