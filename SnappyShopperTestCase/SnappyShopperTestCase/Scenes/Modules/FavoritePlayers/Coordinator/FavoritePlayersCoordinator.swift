//
//  FavoriteCoordinator.swift
//  SnappyShopperTestCase
//
//  Created by Onur Yıldırım on 23.09.2021.
//

import SwiftUI

final class FavoritePlayersCoordinator: ObservableObject, Identifiable {
    
    @Published var viewModel: FavoritePlayersViewModel!
    
    init(title: String) {
        self.viewModel = FavoritePlayersViewModel(title: title, coordinator: self)
    }
}
