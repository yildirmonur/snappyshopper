//
//  FavoritePlayersCoordinatorView.swift
//  SnappyShopperTestCase
//
//  Created by Onur Yıldırım on 23.09.2021.
//

import SwiftUI

struct FavoritePlayersCoordinatorView: View {
    
    // MARK: Stored Properties
    
    @ObservedObject var coordinator: FavoritePlayersCoordinator
    
    // MARK: Views
    
    var body: some View {
        NavigationView {
            FavoritePlayersView(viewModel: coordinator.viewModel)
        }
    }
}

