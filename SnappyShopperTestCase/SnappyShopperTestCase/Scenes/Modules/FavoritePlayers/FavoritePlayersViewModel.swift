//
//  FavoritePlayersViewModel.swift
//  SnappyShopperTestCase
//
//  Created by Onur Yıldırım on 23.09.2021.
//

import Foundation

final class FavoritePlayersViewModel: ObservableObject {
    
    @Published var title: String
    @Published var players: [Player] = []
    
    private unowned let coordinator: FavoritePlayersCoordinator
    
    init(title: String, coordinator: FavoritePlayersCoordinator) {
        self.title = title
        self.coordinator = coordinator
    }
    
    func fetchFavoritePlayers() {
        PersistenceController.shared.fetchPlayers { players in
            DispatchQueue.main.async {
                self.players = players
            }
        }
    }
    
    func delete(at offsets: IndexSet) {
        for index in offsets {
            PersistenceController.shared.delete(with: players[index]) { [weak self] success in
                DispatchQueue.main.async {
                    if success { self?.players.remove(at: index) }
                }
            }
        }
    }
}
